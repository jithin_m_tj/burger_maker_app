Experience the Application using the following link

https://my-react-app-9eb97.web.app/




Sign Up and Login Screen

![Scheme](screenshot/signup_login_screen.png)




Burger Creation Initial View
![Scheme](screenshot/burger_creation_initial_view.png)




Ingredient Control Menu
![Scheme](screenshot/igredient_control.png)




Created burger
![Scheme](screenshot/my_burger.png)




Price Deatils and Order Now Button
![Scheme](screenshot/price_and_order.png)




Order Summary Model
![Scheme](screenshot/order_summary_model.png)




My Burger and Checkout Option.
![Scheme](screenshot/myburger_checkout.png)




Contact Deails Form
![Scheme](screenshot/contact_details_form.png)




Sample Contact Details
![Scheme](screenshot/sample_contact_details.png)




My order Page
![Scheme](screenshot/my_order_page.png)


