import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://my-react-app-9eb97.firebaseio.com/'
});

export default instance;
